"use strict";

class Entities {
  constructor(strenght, psychic, agility, pv, maxPv, actionPoint, maxActionPoint, turn = false,comportment) {
    this.strenght = strenght;
    this.psychic = psychic;
    this.agility = agility;
    this.pv = pv;
    this.maxPv = maxPv;
    this.actionPoint = actionPoint;
    this.maxActionPoint = maxActionPoint;
    this.turn = turn;
    this.comportment = comportment;
  }
  turn() {
    if (this.actionPoint === 0) {
      this.turn = false;
    }
  }

  death() {
    if (this.pv <= 0) {
      this.turn = false;
    }
  }
}
# Talents

> Les talents sont passifs.
>
> Les skills/magie et les enchant peuvent créer des talents (temporaire dans certains cas).

| Nom              | Effet |
| ---------------- | ----- |
| Peau de pierre   |       |
| Cannibale        |       |
| Charognard       |       |
| Vampirisme       |       |
| Reflexe défensif |       |
| Agilité féline   |       |
| Affinité magique |       |

## Catégorie

| Catégorie | Effet                       | Condition | Exemple | Coût |
| --------- | --------------------------- | --------- | ------- | ---- |
| Maîtrise  | Change le seuil de réussite |           | Force   |      |

* À chaque test de force ? /!\
* A chaque test pour une attaque necessitant la force ?


# Système de combat : Jess

## Plusieurs actions/round

On pourrait avoir un nombre de points d'actions définis (genre 5 ou 8 ou que sais-je) pour l'ensemble du combat. L'attaquant comme le défenseur auraient un nombre de points d'actions définis par une carac (DEX?) et par round chacun pourrait ne dépenser qu'un seul point d'action comme tout ses points si il le souhaite. En revanche, les points ne se régénèreraient pas entièrement à chaque round. On pourrait partir sur une base de max 2 points récupérés par round (à voir si ça se régénère en fonction d'une carac, d'un talent ou autre. Je partirai bien sur on régénère 1point/round et si on prend le talent "actif" on peut régénérer 2points/round).
Une action simple ferait dépenser un point d'action et des super attaques consommeraient, elles genre 5points. Il y aurait comme ça un côté tactique en fonction de l'opposant. Par la suite on pourrait même décider d'ajouter des consommables qui augmente la regen de PA et peut etre des poisons qui diminuent les PA.  
Avec cette possibilité, on peut risquer tout ses PA pour tenter d'OS un mob.

("essouflement" pour combats suivant si on a dépenser tout ses PA pour les combats suivants => servira à equilibrer les combats afin qu'on ne puisse pas dépenser 10PA 20 combats d'affilé).

## Une action/round

Ici, on part sur le même système que plus haut. Par contre on aurait qu'une seule action/round. Dans ce cas, je pense que ça serait pas mal de diminuer le nombre de points d'action/combat et la regen/round. On garde comme ça le côté tactique.